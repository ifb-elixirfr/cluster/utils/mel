#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import click
import copy
import ldap
import ldap.modlist
import os
import sys
import unicodedata
# import re
import imaplib
import pprint
import fabric

import paramiko
from paramiko.ssh_exception import AuthenticationException, BadHostKeyException, SSHException

# from string import Template
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

# Add an exception for brazil
print(f"Paramiko Version:{paramiko.__version__}")
_preferred_pubkeys = (
    "ssh-rsa",
    "ssh-ed25519",
    "ecdsa-sha2-nistp256",
    "ecdsa-sha2-nistp384",
    "ecdsa-sha2-nistp521",
    "rsa-sha2-512",
    "rsa-sha2-256",
    "ssh-dss",
)
paramiko.transport.Transport._preferred_pubkeys = _preferred_pubkeys

if os.path.exists('conf.yml'):
    config_path = 'conf.yml'
elif os.path.exists('/etc/mel/conf.yml'):
    config_path = '/etc/mel/conf.yml'
else:
    click.echo("No config file found")
    sys.exit(1)

config = load(open(config_path, 'r'), Loader=Loader)


@click.group()
@click.version_option()
def mel():
    # check if ldap is reachable
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    except ldap.LDAPError as e:
        click.echo("Unable to access ldap server")
        click.echo("Please make sure your VPN connection is enabled")
        click.echo(str(e))
        sys.exit(1)
    ldap_connection.unbind_s()


@mel.command()
@click.option('--uid', default=None)
@click.option('--cn', default=None)
@click.option('--ou', default=None)
@click.option('--objc', default=None)
@click.option('--filt', default=None)
def show_ldap(uid, cn, ou, objc, filt):
    """ show ldap info """

    ldap_filter = None
    if uid:
        ldap_filter = '(uid=' + uid + ')'
    if cn:
        ldap_filter = '(cn=' + cn + ')'
    if ou:
        ldap_filter = '(ou:dn:=' + ou + ')'
    if objc:
        ldap_filter = '(objectClass=' + objc + ')'
    if filt:
        ldap_filter = filt

    if ldap_filter:
        click.echo("Search For " + ldap_filter)
        try:
            ldap_connection = ldap.initialize(config['ldap_server'])
            ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
            results = ldap_connection.search_s(config['ldap_base_dn'], ldap.SCOPE_SUBTREE, ldap_filter)
            ldap_connection.unbind_s()
            click.echo("Find:")
            click.echo("#" * 80)
            for res in results:
                click.echo(str(res))
                click.echo("#" * 80)
        except ldap.LDAPError as e:
            click.echo(str(e))
            sys.exit(1)


@mel.command()
@click.argument('username')
@click.option('--usemail', is_flag=True)
@click.option('--remove', is_flag=True)
def create_user_aliases(username, usemail, remove):
    """ Create a postfix user aliases on the Abims Cluster infrastructure """
    user = get_user(username)
    aliases_name = strip_accents(user['firstname'].lower() + '.' + user['lastname']).lower().replace(" ", "")

    # usemail flag is for external user
    drop = username
    if usemail:
        drop = user['email']

    if _aliases_exists(aliases_name):
        if remove:
            _remove_aliases(aliases_name)
        else:
            click.echo("Aliases already used in ldap")
            sys.exit(1)

    _create_postfix_aliases(aliases_name, [aliases_name, username], [drop])


@mel.command()
@click.argument('groupname')
@click.option('--remove', is_flag=True)
def create_group_aliases(groupname, remove):
    """ Create a postfix group aliases on the Abims Cluster infrastructure """

    ldap_group = _get_group(groupname)
    dn, entry = ldap_group[0]  # todo check if only one result

    aliases_name = entry['cn'][0].decode()

    if _aliases_exists(aliases_name):
        if remove:
            _remove_aliases(aliases_name)
        else:
            click.echo("Aliases already used in ldap")
            sys.exit(1)
    drop = list(map(lambda a: a.decode(), entry['memberUid']))

    _create_postfix_aliases(aliases_name, [aliases_name], drop)


@mel.command()
@click.argument('name')
def remove_aliases(name):
    """ Remove a postfix aliases on the Abims Cluster infrastructure """

    return _remove_aliases(name)


# todo: add a modification method
@mel.command()
@click.argument('name')
@click.option('--accept', multiple=True)
@click.option('--drop', multiple=True)
def create_postfix_aliases(name, accept, drop):
    """ Create a postfix alias on the Abims Cluster infrastructure """

    return _create_postfix_aliases(name, list(accept), list(drop))


# https://docs.python.org/3/library/imaplib.html#imap4-example
@mel.command()
@click.argument('name')
def show_mailbox(name):
    """ Show Mailbox Quota and Acl on the Abims Cluster infrastructure """
    _connect_imap()
    mlb = config['imap_mailbox_prefix'] + name
    try:
        click.echo("ACL:")
        pprint.pprint(imap_cnx.getacl(mlb)[1])
        click.echo("Folder QUOTA:")
        pprint.pprint(imap_cnx.getquotaroot(mlb)[1])
        click.echo("Root QUOTA:")
        prefix_sep = config['imap_mailbox_prefix'][-1]
        mlb_root = prefix_sep.join(mlb.split(prefix_sep)[0:2])
        pprint.pprint(imap_cnx.getquota(mlb_root)[1])
    except imaplib.IMAP4.error as e:
        click.echo("Unable to get mailbox")
        click.echo(str(e))
        sys.exit(1)


@mel.command()
def list_mailbox():
    """ List Mailbox on the Abims Cluster infrastructure """
    _connect_imap()
    try:
        pprint.pprint(imap_cnx.list())
    except imaplib.IMAP4.error as e:
        click.echo("Unable to get mailbox")
        click.echo(str(e))
        sys.exit(1)


@mel.command()
@click.argument('name')
def create_mailbox(name):
    """ Create Mailbox on the Abims Cluster infrastructure """
    # todo : find if we need to set name as user/name pr user.name
    # todo : find if we need to manage part
    mlb = config['imap_mailbox_prefix'] + name
    _create_mailbox(mlb)
    _set_mailbox_quota(mlb, config['imap_default_quota'])


@mel.command()
@click.argument('name')
def remove_mailbox(name):
    """ Remove Mailbox on the Abims Cluster infrastructure """
    mlb = config['imap_mailbox_prefix'] + name
    _remove_mailbox(mlb)


@mel.command()
@click.argument('name')
@click.option('--quota', default=None)
def set_mailbox_quota(name, quota):
    """ Set Mailbox Quota on the Abims Cluster infrastructure """
    mlb = config['imap_mailbox_prefix'] + name
    if quota is None:
        quota = config['imap_default_quota']
    _set_mailbox_quota(mlb, quota)


@mel.command()
@click.argument('name')
@click.option('--password', default=None)
def add_samba(name, password):
    """ Create Mailbox on the Abims Cluster infrastructure """
    # add samba entries in ldap
    _add_samba(name)
    # update samba password
    if password:
        # ugly hack copied from perl script
        # todo put this in template (or not)
        command = "echo -e \'" + password + "\''\n'\'" + password + "\''\n' | /usr/sbin/smbldap-passwd " + name
        run_command(config['samba_server'], command)


def _add_samba(username):
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        user_dn, user_attrs = ldap_connection.search_s(config['ldap_base_dn'], ldap.SCOPE_SUBTREE, 'uid='+username)[0]
        user_uid = user_attrs['uidNumber'][0].decode()

        domain_dn, domain_attrs = ldap_connection.search_s(config['ldap_base_dn'], ldap.SCOPE_SUBTREE,
                                                           '(&(objectClass=sambaDomain)(objectClass=sambaUnixIdPool))')[0]
        domain_sid = domain_attrs['sambaSID'][0].decode()

        # https://mail.python.org/pipermail/python-ldap/2005q1/001504.html
        sambaAttrs = copy.deepcopy(user_attrs)
        if 'sambaSamAccount'.upper() not in (objc.decode().upper() for objc in sambaAttrs['objectClass']):
            sambaAttrs['objectClass'].append('sambaSamAccount'.encode())
        sambaSID = domain_sid + "-" + str(int(user_uid) * 2 + 1000)
        sambaAttrs['sambaSID'] = [sambaSID.encode()]
        sambaAttrs['sambaAcctFlags'] = ['[U]'.encode()]
        modlist = ldap.modlist.modifyModlist(user_attrs, sambaAttrs)

        ldap_connection.modify_s(user_dn, modlist)
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo(str(e))
        sys.exit(1)


def _set_mailbox_quota(name, quota):
    _connect_imap()
    try:
        prefix_sep = config['imap_mailbox_prefix'][-1]
        mlb_root = prefix_sep.join(name.split(prefix_sep)[0:2])
        # https://tools.ietf.org/id/draft-melnikov-extra-quota-00.html#rfc.section.4.1.3
        quota_limits = '(STORAGE ' + str(quota) + ')'
        imap_cnx.setquota(mlb_root, quota_limits)

    except imaplib.IMAP4.error as e:
        click.echo("Unable to set quota on mailbox")
        click.echo(str(e))
        sys.exit(1)


def _create_mailbox(name):
    _connect_imap()
    try:
        imap_cnx.create(name)
        for folder in config['imap_mailbox_folder']:
            imap_cnx.create(name + '.' + folder)
        # todo find if we need to set acl here
    except imaplib.IMAP4.error as e:
        click.echo("Unable to create mailbox")
        click.echo(str(e))
        sys.exit(1)


def _remove_mailbox(name):
    _connect_imap()
    try:
        # delete acl should not be needed on delete
        # imap_cnx.deleteacl(name,config['imap_admin_user'])
        imap_cnx.delete(name)
    except imaplib.IMAP4.error as e:
        click.echo("Unable to remove mailbox")
        click.echo(str(e))
        sys.exit(1)


def _connect_imap():
    global imap_cnx
    imap_cnx = _login_imap()


def _login_imap():
    try:
        imap_connection = imaplib.IMAP4(config['imap_server'])
        imap_connection.login(config['imap_admin_user'], config['imap_admin_password'])
    except imaplib.IMAP4.error as e:
        click.echo("Unable to connect to imap server")
        click.echo(str(e))
        sys.exit(1)
    return imap_connection


def _remove_aliases(name):
    ou = config['ldap_aliases_ou']
    dn = 'cn=%s,%s' % (name, ou)

    if not _aliases_exists(name):
        click.echo("Aliases does not exist in ldap")
        sys.exit(1)

    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        ldap_connection.delete_s(dn)
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Unable to delete aliases")
        click.echo(str(e))
        sys.exit(1)


def _create_postfix_aliases(name, accept, drop):
    aliases_name = name.replace(' ', '').lower()
    ou = config['ldap_aliases_ou']
    dn = 'cn=%s,%s' % (aliases_name, ou)

    if _aliases_exists(aliases_name):
        click.echo("Aliases already used in ldap")
        sys.exit(1)

    modlist = {
        "objectClass": ["myPostfixAlias".encode(), "top".encode()],
        'cn': [aliases_name.encode()],
        'mailacceptinggeneralid': list(map(lambda a: a.encode(), accept)),
        'maildrop': list(map(lambda d: d.encode(), drop)),
    }
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        ldap_connection.add_s(dn, ldap.modlist.addModlist(modlist))
    except ldap.LDAPError as e:
        click.echo("Unable to create ldap aliases")
        click.echo(str(e))
        sys.exit(1)
    ldap_connection.unbind_s()


def _get_aliases(name):
    res = []
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        res = ldap_connection.search_s(base=config['ldap_base_dn'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='(&(objectClass=myPostfixAlias)(cn=%s))' % name)
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo(str(e))
        res = []

    return res


def _aliases_exists(name):
    res = _get_aliases(name)
    return len(res) > 0


def _get_group(name):
    res = []
    try:
        ldap_connection = ldap.initialize(config['ldap_server'])
        ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
        res = ldap_connection.search_s(base=config['ldap_base_dn'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='(&(objectClass=posixGroup)(cn=%s))' % name)
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo(str(e))
        res = []

    return res


def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')


def get_user(username):
    """ retrieve user's firstname from ldap """
    ldap_connection = ldap.initialize(config['ldap_server'])
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])
    results = ldap_connection.search_s(config['ldap_base_dn'], ldap.SCOPE_SUBTREE, 'uid='+username, ['givenName', 'sn', 'mail'])
    ldap_connection.unbind_s()
    if len(results) > 0:
        return {'firstname': results[0][1].get('givenName')[0].decode('utf-8'),
                'lastname': results[0][1].get('sn')[0].decode('utf-8'),
                'email': results[0][1].get('mail')[0].decode('utf-8')}
    else:
        return None


def run_command(server, command, hide=False, warn=False):
    try:
        client = fabric.Connection(server, user='root', connect_timeout=10)
        if server == 'localhost':
            res = client.local(command, hide=hide, warn=warn, pty=True)
        else:
            res = client.run(command, hide=hide, warn=warn, pty=True, in_stream=False)
            client.close()
        return res
    except AuthenticationException as e:
        click.echo(str(e))
        sys.exit(1)
    except BadHostKeyException as e:
        click.echo(str(e))
        sys.exit(1)
    except SSHException as e:
        click.echo(str(e))
        sys.exit(1)
    finally:
        client.close()
