# NNCR cluster User Manager

## Install for developer

Clone the repository

```
git clone repository_url.git
```

`cd` to the repository folder

Create a python virtual environment for the project

```
python3 -m venv env
```

Activate the python virtual environment

```
source env/bin/activate
```

Install the package in development mode

```
pip install -e .
```

## Usage


### Help
```
mel --help
```
