import os

from setuptools import setup

LONG_DESC = open(os.path.join(os.path.dirname(__file__), "README.md")).read()

setup(
    name="mel",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description="User Mail Manager",
    long_description=LONG_DESC,
    author="NNCR Taskforce",
    author_email="gt-nncr-cluster@groupes.france-bioinformatique.fr",
    license="GNU GENERAL PUBLIC LICENSE v2",
    url="",
    zip_safe=False,
    install_requires=[
        'click==8.0',
        'fabric==2.6',
        'passlib==1.7',
        'python-ldap==3.4',
        'PyYAML==6.0',
        'requests==2.27'
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        'Natural Language :: English',
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
    ],
    py_modules=['mel'],
    entry_points={
        'console_scripts': [
            'mel = mel:mel'
        ]
    }
)
